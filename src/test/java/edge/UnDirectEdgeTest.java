package edge;

import lib.edge.impl.UnDirectEdge;
import lib.vertex.Vertex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertTrue;

public class UnDirectEdgeTest {

    private UnDirectEdge unDirectEdge;

    @Before
    public void initTest() {
        unDirectEdge = new UnDirectEdge(new Vertex<>("A"), new Vertex<>("B"));
    }

    @After
    public void afterTest() {
        unDirectEdge = null;
    }

    @Test
    public void testGetFirstVertex() throws Exception {
        assertEquals(new Vertex<>("A"), unDirectEdge.getFirstVertex());
        assertNotEquals(new Vertex<>("B"), unDirectEdge.getFirstVertex());
    }

    @Test
    public void testIsFirstVertex() throws Exception {
        assertTrue(unDirectEdge.isFirstVertex(new Vertex<>("A")));
        assertTrue(unDirectEdge.isFirstVertex(new Vertex<>("B")));
    }

    @Test
    public void testGetSecondVertex() throws Exception {
        assertEquals(new Vertex<>("B"), unDirectEdge.getSecondVertex());
        assertNotEquals(new Vertex<>("A"), unDirectEdge.getSecondVertex());
    }

    @Test
    public void testIsSecondVertex() throws Exception {
        assertTrue(unDirectEdge.isSecondVertex(new Vertex<>("B")));
        assertTrue(unDirectEdge.isSecondVertex(new Vertex<>("A")));
    }
}
