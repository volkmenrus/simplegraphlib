package edge;

import lib.edge.impl.DirectEdge;
import lib.vertex.Vertex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class DirectEdgeTest {

    private DirectEdge directEdge;

    @Before
    public void initTest() {
        directEdge = new DirectEdge(new Vertex<>("A"), new Vertex<>("B"));
    }

    @After
    public void afterTest() {
        directEdge = null;
    }

    @Test
    public void testGetFirstVertex() throws Exception {
        assertEquals(new Vertex<>("A"), directEdge.getFirstVertex());
        assertNotEquals(new Vertex<>("B"), directEdge.getFirstVertex());
    }

    @Test
    public void testIsFirstVertex() throws Exception {
        assertTrue(directEdge.isFirstVertex(new Vertex<>("A")));
        assertFalse(directEdge.isFirstVertex(new Vertex<>("B")));
    }

    @Test
    public void testGetSecondVertex() throws Exception {
        assertEquals(new Vertex<>("B"), directEdge.getSecondVertex());
        assertNotEquals(new Vertex<>("A"), directEdge.getSecondVertex());
    }

    @Test
    public void testIsSecondVertex() throws Exception {
        assertTrue(directEdge.isSecondVertex(new Vertex<>("B")));
        assertFalse(directEdge.isSecondVertex(new Vertex<>("A")));
    }
}
