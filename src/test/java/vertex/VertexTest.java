package vertex;

import lib.vertex.Vertex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class VertexTest {

    private Vertex vertex;

    @Before
    public void initTest() {
        vertex = new Vertex<>("A");
    }

    @After
    public void afterTest() {
        vertex = null;
    }

    @Test
    public void testGetVertex() throws Exception {
        assertEquals(new Vertex<>("A").getVertex() , vertex.getVertex());
        assertNotEquals(new Vertex<>("B").getVertex(), vertex.getVertex());
    }

    @Test
    public void testEquals() throws Exception {
        assertTrue(vertex.equals(new Vertex<>("A")));
        assertTrue(new Vertex<>("A").equals(vertex));
        assertFalse(vertex.equals(new Vertex<>("B")));
        assertFalse(new Vertex<>("B").equals(vertex));
    }

    @Test
    public void testHashCode() throws Exception {
        assertTrue(vertex.hashCode()  == vertex.hashCode());
        assertTrue(vertex.hashCode()  == (new Vertex<>("A").hashCode()));
        assertFalse(vertex.hashCode()  == (new Vertex<>("B").hashCode()));
    }
}
