package graph;

import lib.edge.impl.UnDirectEdge;
import lib.graph.impl.UnDirectedGraph;
import lib.vertex.Vertex;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.*;

public class UnDirectedGraphTest {

    private UnDirectedGraph unDirectedGraph;

    @Before
    public void initTest() {
        unDirectedGraph = new UnDirectedGraph();
        unDirectedGraph.addEdge(new Vertex<>("A"), new Vertex<>("B"));
        unDirectedGraph.addEdge(new Vertex<>("B"), new Vertex<>("D"));
        unDirectedGraph.addEdge(new Vertex<>("B"), new Vertex<>("M"));
        unDirectedGraph.addEdge(new Vertex<>("B"), new Vertex<>("L"));
        unDirectedGraph.addEdge(new Vertex<>("L"), new Vertex<>("E"));
        unDirectedGraph.addEdge(new Vertex<>("A"), new Vertex<>("C"));
        unDirectedGraph.addEdge(new Vertex<>("C"), new Vertex<>("O"));
        unDirectedGraph.addEdge(new Vertex<>("C"), new Vertex<>("K"));
    }

    @After
    public void afterTest() {
        unDirectedGraph = null;
    }

    @Test
    public void testHasVertex() throws Exception {
        assertTrue(unDirectedGraph.hasVertex(new Vertex<>("A")));
        assertFalse(unDirectedGraph.hasVertex(new Vertex<>("T")));
    }

    @Test
    public void testAddVertex() throws Exception {
        unDirectedGraph.addVertex(new Vertex<>("Z"));
        assertTrue(unDirectedGraph.hasVertex(new Vertex<>("Z")));
        assertFalse(unDirectedGraph.hasVertex(new Vertex<>("T")));
    }

    @Test
    public void testHasEdge() throws Exception {
        assertTrue(unDirectedGraph.hasEdge(new Vertex<>("A"), new Vertex<>("B")));
        assertFalse(unDirectedGraph.hasEdge(new Vertex<>("B"), new Vertex<>("A")));
    }

    @Test
    public void testAddEdge() throws Exception {
        unDirectedGraph.addEdge(new Vertex<>("K"), new Vertex<>("Z"));
        assertTrue(unDirectedGraph.hasEdge(new Vertex<>("K"), new Vertex<>("Z")));
        assertFalse(unDirectedGraph.hasEdge(new Vertex<>("Z"), new Vertex<>("K")));
        assertFalse(unDirectedGraph.hasEdge(new Vertex<>("T"), new Vertex<>("Z")));
    }

    @Test
    public void testGetPath() throws Exception {
        assertEquals(Arrays.asList(new UnDirectEdge(new Vertex<>("A"), new Vertex<>("B"))),
                unDirectedGraph.getPath(new Vertex<>("A"), new Vertex<>("B")));
        assertEquals(Arrays.asList(
                new UnDirectEdge(new Vertex<>("A"), new Vertex<>("B")),
                new UnDirectEdge(new Vertex<>("B"), new Vertex<>("L")),
                new UnDirectEdge(new Vertex<>("L"), new Vertex<>("E"))),
                unDirectedGraph.getPath(new Vertex<>("A"), new Vertex<>("E")));
        assertEquals(Arrays.asList(
                new UnDirectEdge(new Vertex<>("A"), new Vertex<>("C")),
                new UnDirectEdge(new Vertex<>("C"), new Vertex<>("K"))),
                unDirectedGraph.getPath(new Vertex<>("A"), new Vertex<>("K")));
        assertEquals(Arrays.asList(), unDirectedGraph.getPath(new Vertex<>("B"), new Vertex<>("T")));
    }
}
