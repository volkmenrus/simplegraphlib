package graph;

import lib.edge.impl.DirectEdge;
import lib.graph.impl.DirectedGraph;
import lib.vertex.Vertex;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;

public class DirectedGraphTest {

    private DirectedGraph directedGraph;

    @Before
    public void initTest() {
        directedGraph = new DirectedGraph();
        DirectedGraph directedGraph = new DirectedGraph();
        directedGraph.addEdge(new Vertex<>("A"), new Vertex<>("B"));
        directedGraph.addEdge(new Vertex<>("B"), new Vertex<>("D"));
        directedGraph.addEdge(new Vertex<>("B"), new Vertex<>("M"));
        directedGraph.addEdge(new Vertex<>("B"), new Vertex<>("L"));
        directedGraph.addEdge(new Vertex<>("B"), new Vertex<>("C"));
        directedGraph.addEdge(new Vertex<>("L"), new Vertex<>("E"));
        directedGraph.addEdge(new Vertex<>("A"), new Vertex<>("C"));
        directedGraph.addEdge(new Vertex<>("C"), new Vertex<>("O"));
        directedGraph.addEdge(new Vertex<>("C"), new Vertex<>("K"));
    }

    @Test
    public void testHasVertex() throws Exception {
        assertTrue(directedGraph.hasVertex(new Vertex<>("A")));
        assertFalse(directedGraph.hasVertex(new Vertex<>("T")));
    }

    @Test
    public void testAddVertex() throws Exception {
        directedGraph.addVertex(new Vertex<>("Z"));
        assertTrue(directedGraph.hasVertex(new Vertex<>("Z")));
        assertFalse(directedGraph.hasVertex(new Vertex<>("T")));
    }

    @Test
    public void testHasEdge() throws Exception {
        assertTrue(directedGraph.hasEdge(new Vertex<>("A"), new Vertex<>("B")));
        assertFalse(directedGraph.hasEdge(new Vertex<>("B"), new Vertex<>("A")));
    }

    @Test
    public void testAddEdge() throws Exception {
        directedGraph.addEdge(new Vertex<>("K"), new Vertex<>("Z"));
        assertTrue(directedGraph.hasEdge(new Vertex<>("K"), new Vertex<>("Z")));
        assertFalse(directedGraph.hasEdge(new Vertex<>("Z"), new Vertex<>("K")));
        assertFalse(directedGraph.hasEdge(new Vertex<>("T"), new Vertex<>("Z")));
    }

    @Test
    public void testGetPath() throws Exception {
        assertEquals(Arrays.asList(new DirectEdge(new Vertex<>("A"), new Vertex<>("B"))),
                directedGraph.getPath(new Vertex<>("A"), new Vertex<>("B")));
        assertEquals(Arrays.asList(
                new DirectEdge(new Vertex<>("A"), new Vertex<>("B")),
                new DirectEdge(new Vertex<>("B"), new Vertex<>("L")),
                new DirectEdge(new Vertex<>("L"), new Vertex<>("E"))),
                directedGraph.getPath(new Vertex<>("A"), new Vertex<>("E")));
        assertEquals(Arrays.asList(
                new DirectEdge(new Vertex<>("A"), new Vertex<>("C")),
                new DirectEdge(new Vertex<>("C"), new Vertex<>("K"))),
                directedGraph.getPath(new Vertex<>("A"), new Vertex<>("K")));
        assertEquals(Arrays.asList(), directedGraph.getPath(new Vertex<>("B"), new Vertex<>("T")));
    }
}
