package lib.graph.impl;

import lib.edge.Edge;
import lib.edge.impl.UnDirectEdge;
import lib.graph.Graph;
import lib.vertex.Vertex;

import java.util.*;

public class UnDirectedGraph implements Graph {

    public UnDirectedGraph() {
    }

    @Override
    public void addVertex(Vertex vertex) {
        addVertexToList(vertex);
    }

    public Boolean hasVertex(Vertex vertex) {
        return vertexMap.keySet().contains(vertex);
    }

    @Override
    public void addEdge(Vertex startVertex, Vertex endVertex) {
        UnDirectEdge edge = new UnDirectEdge(startVertex, endVertex);
        addVertex(startVertex);
        addVertex(endVertex);
        addEdgeToList(edge);
    }

    public boolean hasEdge(Vertex startVertex, Vertex endVertex) {
        boolean result;
        if (hasVertex(startVertex) && hasVertex(endVertex)) {
            UnDirectEdge edge = new UnDirectEdge(startVertex, endVertex);
            result = existEdgeToVertex(startVertex, edge) && existEdgeToVertex(endVertex, edge);
        } else {
            result = false;
        }
        return result;
    }

    private boolean existEdgeToVertex(Vertex vertex, UnDirectEdge unDirectEdge) {
        return vertexMap.get(vertex).contains(unDirectEdge);
    }

    @Override
    public List<Edge> getPath(Vertex startVertex, Vertex endVertex) {
        HashMap<Vertex, Boolean> visited = new HashMap<>();
        HashMap<Vertex, List<Vertex>> parent = new HashMap<>();
        LinkedList<Vertex> queue = new LinkedList<>();
        Vertex currentVertex;

        visited.put(startVertex, true);
        queue.add(startVertex);

        while (queue.size() != 0) {
            currentVertex = queue.poll();
            Iterator<Edge> iterator = vertexMap.get(currentVertex).iterator();
            while (iterator.hasNext()) {
                Edge next = iterator.next();
                Vertex firstVertex = next.getFirstVertex();
                Vertex secondVertex = next.getSecondVertex();
                addVertexToQueue(visited, parent, queue, currentVertex, firstVertex);
                addVertexToQueue(visited, parent, queue, currentVertex, secondVertex);
            }
        }

        return findPath(visited, endVertex, startVertex, parent);
    }

    protected List<Edge> findPath(HashMap<Vertex, Boolean> visited, Vertex endVertex, Vertex startVertex, HashMap<Vertex, List<Vertex>> parent) {
        List<Edge> path = new ArrayList<>();

        if (!visited.keySet().contains(endVertex)) {
            System.out.println("Path does't exist");
        } else {
            List<Vertex> vertices = parent.get(endVertex);
            Vertex previousVertex = endVertex;
            do {
                if (vertices.contains(startVertex)) {
                    path.add(new UnDirectEdge(startVertex, previousVertex));
                    break;
                }
                path.add(new UnDirectEdge(vertices.get(0), previousVertex));
                previousVertex = vertices.get(0);
                vertices = parent.get(previousVertex);
            } while (true);
            Collections.reverse(path);
        }
        return path;
    }

    private void addVertexToQueue(HashMap<Vertex, Boolean> visited, HashMap<Vertex, List<Vertex>> parent, LinkedList<Vertex> queue, Vertex currentVertex, Vertex firstVertex) {
        if (!firstVertex.equals(currentVertex) && (visited.get(firstVertex) == null || !visited.get(firstVertex))) {
            visited.put(firstVertex, true);
            queue.add(firstVertex);
            if (parent.keySet().contains(firstVertex)) {
                parent.get(firstVertex).add(currentVertex);
            } else {
                parent.put(firstVertex, new LinkedList<>(Arrays.asList(currentVertex)));
            }
        }
    }

    private void addVertexToList(Vertex vertex) {
        if (!hasVertex(vertex)) {
            vertexMap.put(vertex, new LinkedList<>());
        }
    }

    private void addEdgeToList(UnDirectEdge edge) {
        if (!existEdgeToVertex(edge.getFirstVertex(), edge)) {
            vertexMap.get(edge.getFirstVertex()).add(edge);
        }
        if (!existEdgeToVertex(edge.getSecondVertex(), edge)) {
            vertexMap.get(edge.getSecondVertex()).add(edge);
        }
    }
}
