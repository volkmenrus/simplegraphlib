package lib.graph.impl;

import lib.edge.Edge;
import lib.edge.impl.DirectEdge;
import lib.vertex.Vertex;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

public class DirectedGraph extends UnDirectedGraph {

    public DirectedGraph() {
    }

    @Override
    public void addEdge(Vertex startVertex, Vertex endVertex) {
        DirectEdge edge = new DirectEdge(startVertex, endVertex);
        addVertex(startVertex);
        addVertex(endVertex);
        addEdgeToList(edge);
    }

    public boolean hasEdge(Vertex startVertex, Vertex endVertex) {
        boolean result;
        if (hasVertex(startVertex) && hasVertex(endVertex)) {
            DirectEdge edge = new DirectEdge(startVertex, endVertex);
            result = existEdgeToVertex(startVertex, edge) && existEdgeToVertex(endVertex, edge);
        } else {
            result = false;
        }
        return result;
    }

    private boolean existEdgeToVertex(Vertex vertex, DirectEdge directEdge) {
        return vertexMap.get(vertex).contains(directEdge);
    }

    private void addEdgeToList(DirectEdge edge) {
        if (!existEdgeToVertex(edge.getFirstVertex(), edge)) {
            vertexMap.get(edge.getFirstVertex()).add(edge);
        }
        if (!existEdgeToVertex(edge.getSecondVertex(), edge)) {
            vertexMap.get(edge.getSecondVertex()).add(edge);
        }
    }

    @Override
    protected List<Edge> findPath(HashMap<Vertex, Boolean> visited, Vertex endVertex, Vertex startVertex, HashMap<Vertex, List<Vertex>> parent) {
        List<Edge> path = new ArrayList<>();

        if (!visited.keySet().contains(endVertex)) {
            System.out.println("Path does't exist");
        } else {
            List<Vertex> vertices = parent.get(endVertex);
            Vertex previousVertex = endVertex;
            do {
                if (vertices.contains(startVertex)) {
                    path.add(new DirectEdge(startVertex, previousVertex));
                    break;
                }
                path.add(new DirectEdge(vertices.get(0), previousVertex));
                previousVertex = vertices.get(0);
                vertices = parent.get(previousVertex);
            } while (true);
            Collections.reverse(path);
        }
        return path;
    }
}
