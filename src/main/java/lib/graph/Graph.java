package lib.graph;

import lib.edge.Edge;
import lib.vertex.Vertex;

import java.util.HashMap;
import java.util.List;

public interface Graph {

    HashMap<Vertex, List<Edge>> vertexMap = new HashMap<>();

    void addVertex(Vertex vertex);

    void addEdge(Vertex startVertex, Vertex endVertex);

    List<Edge> getPath(Vertex startVertex, Vertex endVertex);
}
