package lib.vertex;

import java.util.Objects;

public class Vertex<T> {

    private T vertex;

    public Vertex(T vertex) {
        this.vertex = vertex;
    }

    public T getVertex() {
        return vertex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vertex<?> vertex1 = (Vertex<?>) o;
        return Objects.equals(vertex, vertex1.vertex);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((vertex == null) ? 0 : vertex.hashCode());
        return result;
    }

    @Override
    public String toString() {
        return "Vertex{" +
                "vertex=" + vertex +
                '}';
    }
}
