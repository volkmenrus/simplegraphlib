package lib.edge;

import lib.vertex.Vertex;

public interface Edge {

    public Vertex getFirstVertex();

    public Boolean isFirstVertex(Vertex vertex);

    public Vertex getSecondVertex();

    public Boolean isSecondVertex(Vertex vertex);
}
