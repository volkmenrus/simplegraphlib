package lib.edge.impl;

import lib.edge.Edge;
import lib.vertex.Vertex;

import java.util.Objects;

public class DirectEdge implements Edge {

    private Vertex firstVertex;
    private Vertex secondVertex;

    public DirectEdge(Vertex firstVertex, Vertex secondVertex) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
    }

    @Override
    public Boolean isFirstVertex(Vertex vertex) {
        return vertex.equals(firstVertex);
    }

    @Override
    public Vertex getFirstVertex() {
        return firstVertex;
    }

    @Override
    public Boolean isSecondVertex(Vertex vertex) {
        return vertex.equals(secondVertex);
    }

    @Override
    public Vertex getSecondVertex() {
        return secondVertex;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DirectEdge that = (DirectEdge) o;
        return Objects.equals(firstVertex, that.firstVertex) &&
                Objects.equals(secondVertex, that.secondVertex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstVertex, secondVertex);
    }

    @Override
    public String toString() {
        return "DirectEdge{" +
                "firstVertex=" + firstVertex +
                ", secondVertex=" + secondVertex +
                '}';
    }
}
