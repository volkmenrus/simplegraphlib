package lib.edge.impl;

import lib.edge.Edge;
import lib.vertex.Vertex;

import java.util.Objects;

public class UnDirectEdge implements Edge {

    private Vertex firstVertex;
    private Vertex secondVertex;

    public UnDirectEdge(Vertex firstVertex, Vertex secondVertex) {
        this.firstVertex = firstVertex;
        this.secondVertex = secondVertex;
    }

    @Override
    public Vertex getFirstVertex() {
        return firstVertex;
    }

    @Override
    public Boolean isFirstVertex(Vertex vertex) {
        return vertex.equals(firstVertex) || vertex.equals(secondVertex);
    }

    @Override
    public Vertex getSecondVertex() {
        return secondVertex;
    }

    @Override
    public Boolean isSecondVertex(Vertex vertex) {
        return vertex.equals(secondVertex) || vertex.equals(firstVertex);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UnDirectEdge that = (UnDirectEdge) o;
        return Objects.equals(firstVertex, that.firstVertex) &&
                Objects.equals(secondVertex, that.secondVertex);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstVertex, secondVertex);
    }

    @Override
    public String toString() {
        return "UnDirectEdge{" +
                "firstVertex=" + firstVertex +
                ", secondVertex=" + secondVertex +
                '}';
    }
}
